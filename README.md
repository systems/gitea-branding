# Gitea branding

Custom scripts to rebrand Gitea to my liking, mostly in images.

See also [Customizing Gitea](https://docs.gitea.io/en-us/customizing-gitea/).

[Download](https://gitea.theorangeone.net/api/packages/sys/generic/gitea-branding/latest/branding.zip)
