#!/usr/bin/env python3

import requests
from cairosvg import svg2png
from pathlib import Path
from shutil import rmtree
import os

GITEA_ROOT = "https://gitea.com"
OUTPUT_DIR = Path(__file__).parent.resolve() / "output"

COLOUR_REPLACE = {
    b"#609926": b"#e85537",
}


def download_file(url: str):
    print("Downloading", os.path.basename(url))
    response = requests.get(url)
    response.raise_for_status()
    return response.content


def main():
    try:
        rmtree(OUTPUT_DIR)
    except FileNotFoundError:
        pass

    OUTPUT_DIR.mkdir(exist_ok=True)

    logo = download_file(f"{GITEA_ROOT}/assets/img/logo.svg")
    favicon = download_file(f"{GITEA_ROOT}/assets/img/favicon.svg")

    print("Rebranding...")
    for old_colour, new_colour in COLOUR_REPLACE.items():
        logo = logo.replace(old_colour, new_colour)
        favicon = favicon.replace(old_colour, new_colour)

    print("Resizing...")
    svg2png(logo, write_to=str(OUTPUT_DIR / "logo.png"), output_height=512, output_width=512)
    svg2png(logo, write_to=str(OUTPUT_DIR / "favicon.png"), output_height=180, output_width=180)

    svg2png(logo, write_to=str(OUTPUT_DIR / "avatar_default.png"), output_height=200, output_width=200)
    svg2png(logo, write_to=str(OUTPUT_DIR / "apple-touch-icon.png"), output_height=200, output_width=200)

    (OUTPUT_DIR / "logo.svg").write_bytes(logo)
    (OUTPUT_DIR / "favicon.svg").write_bytes(favicon)
    (OUTPUT_DIR / "gitea.svg").write_bytes(favicon)

if __name__ == "__main__":
    main()
