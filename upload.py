#!/usr/bin/env python3

import requests
from pathlib import Path
from generate import OUTPUT_DIR
import os
import zipfile
from io import BytesIO

ARTIFACT_NAME = "branding.zip"
OWNER = "sys"
PACKAGE_NAME = "gitea-branding"


def main():
    gitea_url = os.environ["GITEA_URL"]
    gitea_token = os.environ["GITEA_TOKEN"]

    session = requests.Session()
    session.headers["Authorization"] = f"token {gitea_token}"

    print("Building archive...")
    archive = BytesIO()
    with zipfile.ZipFile(archive, "w") as zip_file:
        for output_file in OUTPUT_DIR.rglob("*.*"):
            zip_file.writestr(output_file.name, output_file.read_bytes())

    archive.seek(0)

    print("Deleting existing...")
    delete_response = session.delete(f"{gitea_url}/api/packages/{OWNER}/generic/{PACKAGE_NAME}/latest/{ARTIFACT_NAME}")

    # 404 means we've not uploaded it yet.
    if delete_response.status_code != 404:
        delete_response.raise_for_status()

    print("Uploading...")
    response = session.put(f"{gitea_url}/api/packages/{OWNER}/generic/{PACKAGE_NAME}/latest/{ARTIFACT_NAME}", files={ARTIFACT_NAME: archive})
    response.raise_for_status()



if __name__ == "__main__":
    main()
